package PhilipHertvig.alarm;

import java.util.LinkedList;
import java.util.List;

import PhilipHertvig.time.TimeType;

public class Alarm implements AlarmType
  {
  private boolean active;
  private TimeType time;
  private List<AlarmActionType> handlers = new LinkedList<AlarmActionType>();
  
  public Alarm(TimeType time, AlarmActionType handler)
    {
    setTime(time);
    addAlarmActionHandler(handler);
    active = true;
    }
  
  public void setActive(boolean active)
    {
    this.active = active;
    }

  public boolean isActive()
    {
    return this.active;
    }

  public void setTime(TimeType time)
    {
    this.time = time;
    }

  public TimeType getTime()
    {
    return this.time;
    }

  public void addAlarmActionHandler(AlarmActionType handler)
    {
   handlers.add(handler);
    }

  public void doAlarm()
    {
    if(active)
      for(AlarmActionType handler : handlers) // En "for-each" loop
        handler.alarmActivated();    
    }
  
  public String toString()
    {
    return time.toString();
    }
  }
