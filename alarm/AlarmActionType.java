package PhilipHertvig.alarm;

public interface AlarmActionType
  {
  public void alarmActivated();
  public void alarmDeactivated();
  }
