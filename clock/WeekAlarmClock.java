package PhilipHertvig.clock;

import java.util.Collection;

import PhilipHertvig.alarm.AlarmManager;
import PhilipHertvig.alarm.AlarmType;
import PhilipHertvig.counter.*;
import PhilipHertvig.time.Time;
import PhilipHertvig.time.TimeType;


public class WeekAlarmClock implements AlarmClockType{


	protected SettableCounterType days    = new Counter7();
	protected SettableCounterType hours   = new Counter24(days);
	protected SettableCounterType minutes = new Counter60(hours);
	protected SettableCounterType seconds = new Counter60(minutes);
	
	private AlarmManager alarmManager = new AlarmManager();


	public void tickTack() {
		seconds.count();
		System.out.printf(toString() + "\n");
		alarmManager.checkForAlarm(getTime());
	}

	public void setTime(TimeType time) {
		days.setCount(time.getDay());
		hours.setCount(time.getHour());
		minutes.setCount(time.getMinute());
		seconds.setCount(time.getSecond());
	}

	public void addAlarm(AlarmType larm) {
		alarmManager.addAlarm(larm);
		
	}

	public void removeAlarm(AlarmType alarm) {
		alarmManager.removeAlarm(alarm);
		
	}

	public Collection<AlarmType> getAlarms() {
		alarmManager.getAlarms();
		return alarmManager.getAlarms();
	}

	public TimeType getTime() {
		TimeType time = new Time(days.getCount(),hours.getCount(),minutes.getCount(),seconds.getCount());
		return time;
	}
	
	@Override
	public String toString() {
		TimeType time = new Time(days.getCount(),hours.getCount(),minutes.getCount(),seconds.getCount());
		return time.toString();
	}

}
