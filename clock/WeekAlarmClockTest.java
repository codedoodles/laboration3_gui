package PhilipHertvig.clock;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import PhilipHertvig.alarm.Alarm;
import PhilipHertvig.alarm.AlarmActionText;
import PhilipHertvig.alarm.AlarmActionType;
import PhilipHertvig.time.Time;
import PhilipHertvig.time.TimeType;

public class WeekAlarmClockTest {

		@Test
		public void test() {
			//We create our clock
			WeekAlarmClock myClock = new WeekAlarmClock();
			
			
			//Create alarm
			//We make a alarm time
			TimeType alarmTime = new Time(2,2,2,3);
			//and we make its action
			AlarmActionType action1 = new AlarmActionText();
			//then we combine it
			Alarm larm1 = new Alarm(alarmTime, action1);
			//and add it
			myClock.addAlarm(larm1);
			
			
			//Count allot (1 week + 1 second)
			for(int i = 0 ; i < ((3600)*(24)+3) ; i++) {
				myClock.tickTack();
			}
			
			
			//Set time (2,2,2,2) and tick to set of the alarm
			TimeType time = new Time(2,2,2,2);
			myClock.setTime(time);
			myClock.tickTack();
			
			//check the alarm
			assertEquals("[Wed 02:02:03]",myClock.getAlarms().toString());
			
			//remove the alarm and tick from (2,2,2,2)
			myClock.removeAlarm(larm1);
			myClock.setTime(time);
			myClock.tickTack();
			
		}
}
