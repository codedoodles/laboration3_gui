package PhilipHertvig.clock;
import java.util.Collection;
import PhilipHertvig.alarm.AlarmType;
import PhilipHertvig.time.TimeType;

public interface AlarmClockType
  {
  public void tickTack();
  public void setTime(TimeType time);
  public void addAlarm(AlarmType larm);
  public void removeAlarm(AlarmType alarm);
  public Collection<AlarmType> getAlarms();
  public TimeType getTime();
  public String toString();
  }
