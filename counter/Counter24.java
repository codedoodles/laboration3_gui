package PhilipHertvig.counter;


public class Counter24 extends SettableCounter{

	public Counter24() {
		this(null);
	}
	public Counter24(CounterType next) {
		super(Direction.INCREASING, next, 24);
	}
}