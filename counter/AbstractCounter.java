package PhilipHertvig.counter;


public abstract class AbstractCounter implements CounterType{
	public enum Direction {INCREASING, DECREASING};
	protected int contedValue;
	private boolean isPaused;
	private final boolean IS_CIRCULAR;
	private final int COUNT_SPACE;
	private Direction direction;
	private CounterType nextCounter;
	
	public AbstractCounter(Direction direction, CounterType nextCounter, int countSpace) {
		if(countSpace >= 2) {
			this.COUNT_SPACE = countSpace;
			this.IS_CIRCULAR = true;
		}else {
			this.COUNT_SPACE = 0;
			this.IS_CIRCULAR = false;
		}
	this.direction = direction;
	this.nextCounter = nextCounter;
	}
	
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	public int getCount() {
		return this.contedValue;
	}
	
	public void reset() {
		this.contedValue = 0;
	}
	
	public void pause() {
		this.isPaused = true;
	}
	
	public void resume() {
		this.isPaused = false;
	}
	
	public void count() {
		if (!isPaused) {
			if (direction == Direction.INCREASING) {
				this.contedValue++;
				// System.out.println(this.contedValue);
				if(IS_CIRCULAR && this.contedValue == COUNT_SPACE) {
					this.contedValue = 0;
					if (this.nextCounter != null) {
						this.nextCounter.count();
					}else {System.out.printf("Aint nobody got time for that!\n");}
				}
			}else if(direction == Direction.DECREASING) {
				this.contedValue--;
				if(IS_CIRCULAR && this.contedValue == 0) {
					this.contedValue = COUNT_SPACE;
					if (this.nextCounter != null) {
						this.nextCounter.count();
					}
				}
			}
		}
	}
	
	@Override
	public String toString() {
		String str = Integer.toString(contedValue);
		return str;
	}
}
