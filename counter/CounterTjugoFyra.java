package PhilipHertvig.counter;


public class CounterTjugoFyra extends SettableCounter{

	public CounterTjugoFyra() {
		this(null);
	}
	public CounterTjugoFyra(CounterType next) {
		super(Direction.INCREASING, next, 24);
	}
}