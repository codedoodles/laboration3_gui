package PhilipHertvig.counter;


public class Counter60 extends SettableCounter{

	public Counter60() {
		this(null);
	}
	public Counter60(CounterType next) {
		super(Direction.INCREASING, next, 60);
	}
}