package PhilipHertvig.counter;


public class Counter7 extends SettableCounter{

	public Counter7() {
		this(null);
	}
	public Counter7(CounterType next) {
		super(Direction.INCREASING, next, 7);
	}
}